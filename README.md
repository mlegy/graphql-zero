
# GraphQL-Zero

![demo](https://s4.gifyu.com/images/WhatsApp-Video-2021-03-08-at-931.gif)
## Architecture:
- I followed MVVM architecture.
- There is only one Activity `MainActivity` and 2 fragments `PostsListFragment` and `PostDetailsFragment`.
- Communication between view models and views are done using [`LiveData`](https://developer.android.com/reference/androidx/lifecycle/LiveData).

## Modules:
There is 2 modules:
1. `:core`: which has the main App dependencies and dagger graph.
2. `:app` this module has the single feature of the app (listing posts and post details).

As you can see, the `:core` module is not really needed in this code challenge, I just did it to demonstrate how can I achieve the multi module architecture.

## 3rd Party Libraries
1. **[google/dagger](https://github.com/google/dagger)**: for dependency injection.
2. **[apollographql/apollo-android](https://github.com/apollographql/apollo-android)**: for the graphql integration.
3. **[facebook/shimmer-android](https://github.com/facebook/shimmer-android)**: for shimmer effect in loading states.

## Testing
- Unit tests for the view models: `ListPostsViewModelTest` and `PostDetailsViewModelTest`.
- Unit tests for the mappers: `PostsMapperTest` and `UserMapperTest`.
- UI tests for the fragments: `PostsListFragmentTest` and `PostDetailsFragmentTest`.

![unit-tests-test](https://bitbucket.org/mlegy/graphql-zero/raw/da4349875db2da84ca7e4ac401a5643d07a84390/screenshots/unit-tests-result.png)
![integration-tests-test](https://bitbucket.org/mlegy/graphql-zero/raw/da4349875db2da84ca7e4ac401a5643d07a84390/screenshots/integration-tests-result.png)