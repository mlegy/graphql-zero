plugins {
    id("com.android.library")
    id("kotlin-android")
    id("kotlin-kapt")
}
android {
    compileSdkVersion(30)
    buildToolsVersion("30.0.3")

    defaultConfig {
        minSdkVersion(16)
        targetSdkVersion(30)
    }

    compileOptions {
        sourceCompatibility = JavaVersion.VERSION_1_8
        targetCompatibility = JavaVersion.VERSION_1_8
    }

    kotlinOptions {
        jvmTarget = JavaVersion.VERSION_1_8.toString()
    }
}


dependencies {
    implementation("org.jetbrains.kotlin:kotlin-stdlib:1.4.31")

    implementation("androidx.fragment:fragment-ktx:1.3.0")

    implementation("com.apollographql.apollo:apollo-runtime:2.5.4")
    implementation("com.apollographql.apollo:apollo-coroutines-support:2.5.4")

    implementation("com.google.dagger:dagger:2.31.2")
    kapt("com.google.dagger:dagger-compiler:2.31")
}
