package com.melegy.core.network

import com.apollographql.apollo.ApolloQueryCall
import com.apollographql.apollo.api.Response
import com.apollographql.apollo.coroutines.await
import com.apollographql.apollo.exception.ApolloException
import com.apollographql.apollo.exception.ApolloHttpException

/**
 * Helper extension function that transforms from Apollo [Response]` to [Result] class.
 */
suspend fun <T : Any> ApolloQueryCall<T>.toResult(): Result<T> = try {
    val response: Response<T> = this.await()
    if (response.hasErrors()) {
        val message = response.errors?.map { it.message }?.first()
        Result.Failure.HttpError(message)
    } else {
        val data = requireNotNull(response.data)
        Result.Success(data)
    }
} catch (e: ApolloException) {
    if (e is ApolloHttpException) {
        Result.Failure.HttpError(e.message())
    } else {
        Result.Failure.NetworkError
    }
}
