package com.melegy.core.network

/**
 * A wrapper class for Apollo response.
 */
sealed class Result<out T : Any> {
    data class Success<T : Any>(val data: T) : Result<T>()
    sealed class Failure : Result<Nothing>() {
        object NetworkError : Failure()
        data class HttpError(val message: String?) : Failure()
    }
}
