package com.melegy.core.injection

interface CoreComponentProvider {
    val coreComponent: CoreComponent
}
