package com.melegy.core.injection

import com.apollographql.apollo.ApolloClient
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
internal class CoreModule {
    @Provides
    @Singleton
    fun provideApolloClient(): ApolloClient = ApolloClient.builder()
        .serverUrl("https://graphqlzero.almansi.me/api")
        .build()
}
