package com.melegy.core.injection

import androidx.fragment.app.Fragment

val Fragment.coreComponent get() = ((requireActivity().application) as CoreComponentProvider).coreComponent
