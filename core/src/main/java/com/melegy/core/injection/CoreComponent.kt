package com.melegy.core.injection

import android.app.Application
import com.apollographql.apollo.ApolloClient
import dagger.BindsInstance
import dagger.Component
import javax.inject.Singleton


/**
 * Component providing application wide singletons.
 * To call this make use of the Activity.CoreComponent extension function.
 */
@Singleton
@Component(modules = [CoreModule::class])
interface CoreComponent {

    fun provideApolloClient(): ApolloClient

    @Component.Factory
    interface Factory {
        fun create(@BindsInstance application: Application): CoreComponent

        companion object Default : Factory by DaggerCoreComponent.factory()
    }
}
