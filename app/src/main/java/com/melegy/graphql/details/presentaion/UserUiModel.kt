package com.melegy.graphql.details.presentaion

/**
 * Representation model of the user item.
 */
internal data class UserUiModel(
    val id: String,
    val name: String,
    val username: String
)
