package com.melegy.graphql.details.presentaion

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.melegy.core.network.Result
import com.melegy.graphql.common.data.NetworkService
import kotlinx.coroutines.launch
import javax.inject.Inject

/**
 * ViewModel for the post details fragment.
 * It only exposes one live data [viewState] which emits the view state
 * that should be rendered by the fragment.
 */
internal class PostDetailsViewModel @Inject constructor(private val networkService: NetworkService) :
    ViewModel() {

    private val _viewState = MutableLiveData<ViewState>()
    val viewState: LiveData<ViewState>
        get() = _viewState

    fun getAuthor(userId: String) {
        viewModelScope.launch {
            when (val result = networkService.getUserDetails(userId)) {
                is Result.Success -> {
                    val userRaw = requireNotNull(result.data.user)
                    _viewState.value = ViewState.Data(UserMapper(userRaw))
                }
                is Result.Failure.HttpError ->
                    _viewState.value = ViewState.HttpError(result.message)
                is Result.Failure.NetworkError ->
                    _viewState.value = ViewState.NetworkError
            }
        }
    }
}
