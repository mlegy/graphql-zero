package com.melegy.graphql.details.presentaion

internal sealed class ViewState {
    data class Data(val user: UserUiModel) : ViewState()
    data class HttpError(val message: String?) : ViewState()
    object NetworkError : ViewState()
}
