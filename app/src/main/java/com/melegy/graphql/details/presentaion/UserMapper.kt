package com.melegy.graphql.details.presentaion

import com.melegy.graphql.GetUserQuery

internal object UserMapper {
    operator fun invoke(user: GetUserQuery.User) = UserUiModel(
        id = requireNotNull(user.id),
        name = requireNotNull(user.name),
        username = requireNotNull(user.username),
    )
}
