package com.melegy.graphql.details.ui

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import com.google.android.material.snackbar.Snackbar
import com.melegy.core.injection.coreComponent
import com.melegy.graphql.R
import com.melegy.graphql.common.di.AppComponent
import com.melegy.graphql.databinding.FragmentPostDetailsBinding
import com.melegy.graphql.details.presentaion.PostDetailsViewModel
import com.melegy.graphql.details.presentaion.UserUiModel
import com.melegy.graphql.details.presentaion.ViewState
import com.melegy.graphql.list.persentaion.PostUiModel

internal class PostDetailsFragment : Fragment() {

    private val viewModelFactory by lazy { AppComponent.Factory.create(coreComponent).viewModelFactoryFactory }
    private val viewModel by viewModels<PostDetailsViewModel> { viewModelFactory }

    private val post by lazy {
        requireNotNull(requireArguments().getParcelable<PostUiModel>(EXTRA_POST))
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        val binding = FragmentPostDetailsBinding.inflate(inflater, container, false)
        binding.bindPost(post)
        with(viewModel) {
            getAuthor(post.userId)
            viewState.observe(viewLifecycleOwner) { binding.handleViewState(it) }
        }
        return binding.root
    }

    private fun FragmentPostDetailsBinding.handleViewState(viewState: ViewState) {
        hideLoading()
        when (viewState) {
            is ViewState.Data -> bindUser(viewState.user)
            is ViewState.HttpError -> showError(
                viewState.message ?: getString(R.string.failed_to_user)
            )
            ViewState.NetworkError -> showError(getString(R.string.network_error_message))
        }
    }

    private fun FragmentPostDetailsBinding.bindPost(post: PostUiModel) {
        textTitle.text = post.title
        textBody.text = post.fullBody
    }

    private fun FragmentPostDetailsBinding.bindUser(user: UserUiModel) {
        textAuthorName.text = user.name
        textAuthorUsername.text = user.username
    }

    private fun FragmentPostDetailsBinding.showError(errorMessage: String) {
        Snackbar.make(root, errorMessage, Snackbar.LENGTH_INDEFINITE).apply {
            setAction(R.string.retry) { viewModel.getAuthor(post.userId) }
            show()
        }
    }

    private fun FragmentPostDetailsBinding.hideLoading() {
        shimmerAuthorView.apply {
            hideShimmer()
            isVisible = false
        }
    }

    companion object {
        private const val EXTRA_POST = "EXTRA_POST"
        fun newBundle(post: PostUiModel) = Bundle().apply { putParcelable(EXTRA_POST, post) }
    }
}
