package com.melegy.graphql.common.di

import androidx.lifecycle.ViewModelProvider
import com.melegy.core.injection.CoreComponent
import com.melegy.core.injection.scope.FeatureScope
import dagger.Component

@FeatureScope
@Component(modules = [AppModule::class], dependencies = [CoreComponent::class])
internal interface AppComponent {

    val viewModelFactoryFactory: ViewModelProvider.Factory

    @Component.Factory
    interface Factory {
        fun create(coreComponent: CoreComponent): AppComponent

        companion object Default : Factory by DaggerAppComponent.factory()
    }
}
