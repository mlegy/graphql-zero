package com.melegy.graphql.common.di

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.melegy.core.injection.ViewModelFactory
import com.melegy.core.injection.ViewModelKey
import com.melegy.core.injection.scope.FeatureScope
import com.melegy.graphql.list.persentaion.ListPostsViewModel
import com.melegy.graphql.details.presentaion.PostDetailsViewModel
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

@Module
internal abstract class AppModule {
    @Binds
    @FeatureScope
    @IntoMap
    @ViewModelKey(ListPostsViewModel::class)
    abstract fun bindListPostsViewModel(listPostsViewModel: ListPostsViewModel): ViewModel

    @Binds
    @FeatureScope
    @IntoMap
    @ViewModelKey(PostDetailsViewModel::class)
    abstract fun bindsPostDetailsViewModel(postsViewModel: PostDetailsViewModel): ViewModel

    @Binds
    @FeatureScope
    abstract fun bindViewModelFactory(factory: ViewModelFactory): ViewModelProvider.Factory
}
