package com.melegy.graphql.common.data

import com.apollographql.apollo.ApolloClient
import com.apollographql.apollo.api.toInput
import com.melegy.core.network.toResult
import com.melegy.graphql.GetUserQuery
import com.melegy.graphql.ListPostsQuery
import com.melegy.graphql.type.PageQueryOptions
import com.melegy.graphql.type.PaginateOptions
import javax.inject.Inject

internal class NetworkService @Inject constructor(private val apolloClient: ApolloClient) {
    suspend fun paginatePosts(page: Int) = apolloClient.query(
        ListPostsQuery(
            PageQueryOptions(
                paginate = PaginateOptions(
                    page = page.toInput(),
                    limit = PAGE_LIMIT.toInput()
                ).toInput()
            )
        )
    ).toResult()

    suspend fun getUserDetails(userId: String) = apolloClient.query(GetUserQuery(userId)).toResult()

    private companion object {
        private const val PAGE_LIMIT = 10
    }
}
