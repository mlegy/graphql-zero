package com.melegy.graphql.list.ui

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.fragment.app.commit
import androidx.fragment.app.viewModels
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.DividerItemDecoration.VERTICAL
import com.google.android.material.snackbar.Snackbar
import com.melegy.core.injection.coreComponent
import com.melegy.graphql.R
import com.melegy.graphql.common.di.AppComponent
import com.melegy.graphql.databinding.FragmentListPostsBinding
import com.melegy.graphql.details.ui.PostDetailsFragment
import com.melegy.graphql.list.persentaion.ListPostsViewModel
import com.melegy.graphql.list.persentaion.PostUiModel
import com.melegy.graphql.list.persentaion.ViewState

internal class PostsListFragment : Fragment() {

    private val viewModelFactory by lazy { AppComponent.Factory.create(coreComponent).viewModelFactoryFactory }
    private val viewModel by viewModels<ListPostsViewModel> { viewModelFactory }

    private val postsAdapter = PostsAdapter(
        onLoadMore = { viewModel.onLoadPosts() },
        onPostClick = { navigateTpoPostDetails(it) }
    )

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        val binding = FragmentListPostsBinding.inflate(inflater, container, false)
        val dividerItemDecoration = DividerItemDecoration(requireContext(), VERTICAL)
        with(binding.recyclerViewPosts) {
            adapter = postsAdapter
            addItemDecoration(dividerItemDecoration)
        }
        observeViewState(binding)
        return binding.root
    }

    private fun observeViewState(binding: FragmentListPostsBinding) {
        viewModel.viewState.observe(viewLifecycleOwner) { binding.handleViewState(it) }
    }

    private fun FragmentListPostsBinding.handleViewState(viewState: ViewState) {
        when (viewState) {
            is ViewState.Data -> showPosts(viewState.posts)
            ViewState.InitialLoading -> shimmerLoadingView.isVisible = true
            ViewState.PaginationLoading -> progressbarPagination.isVisible = true
            is ViewState.HttpError -> showError(
                viewState.message ?: getString(R.string.failed_to_load_posts)
            )
            ViewState.NetworkError -> showError(getString(R.string.network_error_message))
        }
    }

    private fun FragmentListPostsBinding.showPosts(posts: List<PostUiModel>) {
        progressbarPagination.isVisible = false
        shimmerLoadingView.isVisible = false
        postsAdapter.submitList(posts)
    }

    private fun FragmentListPostsBinding.showError(errorMessage: String) {
        shimmerLoadingView.isVisible = false
        Snackbar.make(root, errorMessage, Snackbar.LENGTH_INDEFINITE).apply {
            setAction(R.string.retry) { viewModel.onLoadPosts() }
            show()
        }
    }

    private fun navigateTpoPostDetails(post: PostUiModel) {
        parentFragmentManager.commit {
            replace(
                R.id.fragmentContainer,
                PostDetailsFragment::class.java,
                PostDetailsFragment.newBundle(post)
            )
            addToBackStack(null)
        }
    }
}
