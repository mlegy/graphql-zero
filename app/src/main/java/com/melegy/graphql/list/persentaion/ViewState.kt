package com.melegy.graphql.list.persentaion

internal sealed class ViewState() {
    data class Data(val posts: List<PostUiModel>) : ViewState()
    object InitialLoading : ViewState()
    object PaginationLoading : ViewState()
    data class HttpError(val message: String?) : ViewState()
    object NetworkError : ViewState()
}
