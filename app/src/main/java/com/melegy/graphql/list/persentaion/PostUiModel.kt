package com.melegy.graphql.list.persentaion

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

/**
 * Representation model of the post item.
 */
@Parcelize
internal data class PostUiModel(
    val id: String,
    val userId: String,
    val title: String,
    val trimmedBody: String,
    val fullBody: String
) : Parcelable
