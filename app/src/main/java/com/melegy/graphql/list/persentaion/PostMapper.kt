package com.melegy.graphql.list.persentaion

import com.melegy.graphql.ListPostsQuery

internal object PostMapper {
    operator fun invoke(postsRaw: List<ListPostsQuery.Data1>) = postsRaw.map {
        PostUiModel(
            id = requireNotNull(it.id),
            title = requireNotNull(it.title),
            trimmedBody = requireNotNull("${it.body?.take(120)}.."),
            fullBody = requireNotNull(it.body),
            userId = requireNotNull(it.user?.id)
        )
    }
}
