package com.melegy.graphql.list.persentaion

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.melegy.core.network.Result
import com.melegy.graphql.ListPostsQuery
import com.melegy.graphql.common.data.NetworkService
import kotlinx.coroutines.launch
import javax.inject.Inject
import kotlin.properties.Delegates

/**
 * ViewModel for the posts lists fragment.
 * It only exposes one live data [viewState] which emits the view state
 * that should be rendered by the fragment.
 */
internal class ListPostsViewModel @Inject constructor(
    private val networkService: NetworkService
) : ViewModel() {

    private val _viewState = MutableLiveData<ViewState>()
    val viewState: LiveData<ViewState>
        get() = _viewState

    private var pageIndex = 1
    private var hasMorePosts = true

    private var posts: List<PostUiModel> by Delegates.observable(emptyList()) { _, _, new ->
        _viewState.value = ViewState.Data(new)
    }

    init {
        onLoadPosts()
    }

    fun onLoadPosts() {
        if (hasMorePosts) {
            viewModelScope.launch {
                emitLoading()
                when (val response = networkService.paginatePosts(pageIndex)) {
                    is Result.Failure.NetworkError -> emitNetworkError()
                    is Result.Failure.HttpError -> emitHttpError(response.message)
                    is Result.Success -> handleSuccessState(requireNotNull(response.data.posts))
                }
            }
        }
    }

    private fun emitLoading() {
        _viewState.value =
            if (pageIndex == 1) ViewState.InitialLoading else ViewState.PaginationLoading
    }

    private fun emitHttpError(errorMessage: String?) {
        _viewState.value = ViewState.HttpError(errorMessage)
    }

    private fun emitNetworkError() {
        _viewState.value = ViewState.NetworkError
    }

    private fun handleSuccessState(response: ListPostsQuery.Posts) {
        val rawPosts = requireNotNull(response.data?.filterNotNull())
        val count = requireNotNull(response.meta?.totalCount)
        val newPosts = PostMapper(rawPosts)
        updateLocaleState(newPosts, count)
    }

    private fun updateLocaleState(newPosts: List<PostUiModel>, count: Int) {
        posts = posts + newPosts
        hasMorePosts = count > posts.size
        pageIndex += 1
    }
}
