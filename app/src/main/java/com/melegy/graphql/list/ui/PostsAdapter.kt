package com.melegy.graphql.list.ui

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.melegy.graphql.databinding.ItemPostBinding
import com.melegy.graphql.list.persentaion.PostUiModel

internal class PostsAdapter(
    private val onLoadMore: () -> Unit,
    private val onPostClick: (PostUiModel) -> Unit
) : ListAdapter<PostUiModel, PostsAdapter.ViewHolder>(PostDiffCallback) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding = ItemPostBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return ViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(getItem(position), onPostClick)
        if (position == currentList.size - 1) {
            onLoadMore()
        }
    }

    class ViewHolder(private val binding: ItemPostBinding) : RecyclerView.ViewHolder(binding.root) {
        fun bind(post: PostUiModel, onPostClick: (PostUiModel) -> Unit) {
            with(binding) {
                textTitle.text = post.title
                textBody.text = post.trimmedBody
                root.setOnClickListener { onPostClick(post) }
            }
        }
    }

    private object PostDiffCallback : DiffUtil.ItemCallback<PostUiModel>() {
        override fun areItemsTheSame(oldItem: PostUiModel, newItem: PostUiModel) =
            oldItem.id == newItem.id

        override fun areContentsTheSame(oldItem: PostUiModel, newItem: PostUiModel) =
            oldItem == newItem
    }
}
