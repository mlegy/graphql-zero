package com.melegy.graphql

import androidx.multidex.MultiDexApplication
import com.melegy.core.injection.CoreComponent
import com.melegy.core.injection.CoreComponentProvider

internal class GraphqlApp : MultiDexApplication(), CoreComponentProvider {
    override val coreComponent: CoreComponent by lazy {
        CoreComponent.Factory.create(this)
    }
}
