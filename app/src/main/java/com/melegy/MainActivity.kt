package com.melegy

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.commit
import com.melegy.graphql.R
import com.melegy.graphql.databinding.ActivityMainBinding
import com.melegy.graphql.list.ui.PostsListFragment

internal class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
        if (savedInstanceState == null) {
            supportFragmentManager.commit {
                replace(R.id.fragmentContainer, PostsListFragment::class.java, null)
            }
        }
    }
}
