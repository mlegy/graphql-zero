package com.melegy.graphql.details

import androidx.fragment.app.testing.launchFragmentInContainer
import androidx.fragment.app.testing.withFragment
import androidx.test.internal.runner.junit4.AndroidJUnit4ClassRunner
import com.melegy.graphql.R
import com.melegy.graphql.details.ui.PostDetailsFragment
import com.melegy.graphql.list.persentaion.PostUiModel
import com.melegy.graphql.registerApolloClientIdlingResource
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4ClassRunner::class)
class PostDetailsFragmentTest {

    @Before
    fun setup() {
        launchFragmentInContainer<PostDetailsFragment>(
            themeResId = R.style.Theme_Graphql,
            fragmentArgs = PostDetailsFragment.newBundle(
                PostUiModel(
                    "1",
                    "1",
                    "title",
                    "trimmed body",
                    "full body"
                )
            )
        ).withFragment { registerApolloClientIdlingResource() }
    }

    @Test
    fun showPostWithUserInfo() {
        postsDetailsRobot {
            isTitleDisplayed("title")
            isBodyDisplayed("full body")
            isAuthorLabelDisplayed()
            isAuthorNameDisplayed()
            isAuthorUsernameDisplayed()
        }
    }

    @After
    fun tearDown(){

    }
}
