package com.melegy.graphql.details

import com.melegy.graphql.R
import com.schibsted.spain.barista.assertion.BaristaVisibilityAssertions.assertDisplayed
import com.schibsted.spain.barista.assertion.BaristaVisibilityAssertions.assertNotDisplayed

fun postsDetailsRobot(f: PostsDetailsRobot.() -> Unit) = PostsDetailsRobot().also(f)

class PostsDetailsRobot {
    private val titleId = R.id.textTitle
    private val bodyId = R.id.textBody
    private val authorLabelId = R.id.labelAuthor
    private val authorName = R.id.textAuthorName
    private val authorUsername = R.id.textAuthorUsername

    fun isTitleDisplayed(title: String) {
        assertDisplayed(titleId, title)
    }

    fun isBodyDisplayed(body: String) {
        assertDisplayed(bodyId, body)
    }

    fun isAuthorLabelDisplayed() {
        assertDisplayed(authorLabelId, "Author")
    }

    fun isAuthorNameDisplayed() {
        assertDisplayed(authorName)
    }

    fun isAuthorUsernameDisplayed() {
        assertDisplayed(authorUsername)
    }
}
