package com.melegy.graphql

import androidx.fragment.app.Fragment
import androidx.test.espresso.IdlingRegistry
import com.apollographql.apollo.test.espresso.ApolloIdlingResource
import com.melegy.core.injection.coreComponent

fun Fragment.registerApolloClientIdlingResource() {
    val idlingResource = ApolloIdlingResource.create(
        "ApolloIdlingResource",
        this.coreComponent.provideApolloClient()
    )
    IdlingRegistry.getInstance().register(idlingResource)
}
