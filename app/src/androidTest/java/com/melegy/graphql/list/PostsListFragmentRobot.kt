package com.melegy.graphql.list

import com.melegy.graphql.R
import com.schibsted.spain.barista.assertion.BaristaVisibilityAssertions.assertDisplayed

fun postsListFragmentRobot(f: PostsListRobot.() -> Unit) = PostsListRobot().also(f)

class PostsListRobot {
    private val postsRecyclerView = R.id.recyclerViewPosts

    fun isPostsRecyclerViewVisible() {
        assertDisplayed(postsRecyclerView)
    }
}
