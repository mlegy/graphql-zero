package com.melegy.graphql.list

import androidx.fragment.app.testing.launchFragmentInContainer
import androidx.fragment.app.testing.withFragment
import androidx.test.internal.runner.junit4.AndroidJUnit4ClassRunner
import com.melegy.graphql.R
import com.melegy.graphql.list.ui.PostsListFragment
import com.melegy.graphql.registerApolloClientIdlingResource
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4ClassRunner::class)
class PostsListFragmentTest {

    @Before
    fun setup() {
        launchFragmentInContainer<PostsListFragment>(themeResId = R.style.Theme_Graphql).withFragment {
            registerApolloClientIdlingResource()
        }
    }

    @Test
    fun showPosts() {
        launchFragmentInContainer<PostsListFragment>(themeResId = R.style.Theme_Graphql).withFragment {
            registerApolloClientIdlingResource()
        }
        postsListFragmentRobot {
            isPostsRecyclerViewVisible()
        }
    }
}
