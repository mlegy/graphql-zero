package com.melegy.graphql.list.presentation

import com.melegy.graphql.ListPostsQuery
import com.melegy.graphql.list.persentaion.PostMapper
import com.melegy.graphql.list.persentaion.PostUiModel
import junit.framework.Assert.assertEquals
import org.junit.Test

internal class PostsMapperTest {
    @Test
    fun `map posts response to ui model correctly`() {
        val postsRaw = ListPostsQuery.Data1(
            id = "1",
            title = "title",
            body = "quia et suscipit nsuscipit recusandae consequuntur expedita et cum nreprehenderit molestiae ut ut quas totam nnostrum rerum est autem sunt rem eveniet architecto",
            user = ListPostsQuery.User(id = "2")
        )

        val postsUi = PostUiModel(
            id = "1",
            title = "title",
            trimmedBody = "quia et suscipit nsuscipit recusandae consequuntur expedita et cum nreprehenderit molestiae ut ut quas totam nnostrum re..",
            fullBody = "quia et suscipit nsuscipit recusandae consequuntur expedita et cum nreprehenderit molestiae ut ut quas totam nnostrum rerum est autem sunt rem eveniet architecto",
            userId = "2"
        )

        assertEquals(listOf(postsUi), PostMapper(listOf(postsRaw)))
    }
}
