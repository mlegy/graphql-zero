package com.melegy.graphql.list.presentation

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.melegy.core.network.Result
import com.melegy.graphql.ListPostsQuery
import com.melegy.graphql.MainCoroutineRule
import com.melegy.graphql.common.data.NetworkService
import com.melegy.graphql.list.persentaion.ListPostsViewModel
import com.melegy.graphql.list.persentaion.ViewState
import com.nhaarman.mockitokotlin2.*
import kotlinx.coroutines.test.runBlockingTest
import org.hamcrest.CoreMatchers.instanceOf
import org.junit.Assert.assertEquals
import org.junit.Assert.assertThat
import org.junit.Rule
import org.junit.Test

internal class ListPostsViewModelTest {
    @get:Rule
    val instantTaskExecutorRule = InstantTaskExecutorRule()

    @get:Rule
    val coroutineRule = MainCoroutineRule()

    private val networkService = mock<NetworkService>()
    private lateinit var sut: ListPostsViewModel

    @Test
    fun `on create - and network service succeeds - calls get posts`() = runBlockingTest {
        ArrangeBuilder()
            .withGetPostsSuccess(1, postsResponse)

        sut = ListPostsViewModel(networkService)

        verify(networkService).paginatePosts(1)
    }

    @Test
    fun `on load more - and network service succeeds - calls get posts with page index incremented`() =
        runBlockingTest {
            ArrangeBuilder()
                .withGetPostsSuccess(1, postsResponse)

            sut = ListPostsViewModel(networkService)
            sut.onLoadPosts()

            verify(networkService).paginatePosts(2)
        }

    @Test
    fun `on create - and network service succeeds - emits view state`() = runBlockingTest {
        ArrangeBuilder()
            .withGetPostsSuccess(1, postsResponse)

        sut = ListPostsViewModel(networkService)

        val viewState = sut.viewState.value

        assertThat(viewState, instanceOf(ViewState.Data::class.java))
    }

    @Test
    fun `on load more - and network service succeeds - emits view state with new and old posts`() =
        runBlockingTest {
            ArrangeBuilder()
                .withGetPostsSuccess(1, postsResponse)
                .withGetPostsSuccess(2, postsResponse)

            sut = ListPostsViewModel(networkService)
            sut.onLoadPosts()

            val viewState = sut.viewState.value

            assertEquals((viewState as ViewState.Data).posts.size, 2)
        }

    @Test
    fun `on load more - while all items are retrieved - don't call network service`() =
        runBlockingTest {
            val postsResponse = postsResponse.copy(
                posts = postsResponse.posts?.copy(
                    meta = postsResponse.posts?.meta?.copy(totalCount = 1)
                )
            )
            ArrangeBuilder()
                .withGetPostsSuccess(1, postsResponse)

            sut = ListPostsViewModel(networkService)
            sut.onLoadPosts()

            verify(networkService, never()).paginatePosts(2)
        }

    @Test
    fun `on create - and network service emits network error - emits failure view state`() =
        runBlockingTest {
            ArrangeBuilder()
                .withGetPostsFailed(Result.Failure.NetworkError)

            sut = ListPostsViewModel(networkService)

            val viewState = sut.viewState.value

            assertEquals(ViewState.NetworkError, viewState)
        }

    @Test
    fun `on create - and network service emits http error - emits failure view state`() =
        runBlockingTest {
            ArrangeBuilder()
                .withGetPostsFailed(Result.Failure.HttpError("Bang!"))

            sut = ListPostsViewModel(networkService)

            val viewState = sut.viewState.value

            assertEquals(ViewState.HttpError("Bang!"), viewState)
        }

    private inner class ArrangeBuilder {
        suspend fun withGetPostsSuccess(
            pageIndex: Int,
            postsResponse: ListPostsQuery.Data
        ) = also {
            whenever(networkService.paginatePosts(pageIndex))
                .thenReturn(Result.Success(postsResponse))
        }

        suspend fun withGetPostsFailed(error: Result.Failure) = also {
            whenever(networkService.paginatePosts(any()))
                .thenReturn(error)
        }
    }

    private companion object {
        private val postsResponse = ListPostsQuery.Data(
            posts = ListPostsQuery.Posts(
                data = listOf(
                    ListPostsQuery.Data1(
                        id = "1",
                        title = "title",
                        body = "body",
                        user = ListPostsQuery.User(id = "1")
                    )
                ),
                meta = ListPostsQuery.Meta(totalCount = 10)
            )
        )
    }
}
