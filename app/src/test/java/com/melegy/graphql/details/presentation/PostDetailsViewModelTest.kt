package com.melegy.graphql.details.presentation

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.melegy.core.network.Result
import com.melegy.graphql.GetUserQuery
import com.melegy.graphql.MainCoroutineRule
import com.melegy.graphql.common.data.NetworkService
import com.melegy.graphql.details.presentaion.PostDetailsViewModel
import com.melegy.graphql.details.presentaion.ViewState
import com.nhaarman.mockitokotlin2.any
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.verify
import com.nhaarman.mockitokotlin2.whenever
import kotlinx.coroutines.test.runBlockingTest
import org.hamcrest.CoreMatchers.instanceOf
import org.junit.Assert.assertEquals
import org.junit.Assert.assertThat
import org.junit.Rule
import org.junit.Test

internal class PostDetailsViewModelTest {
    @get:Rule
    val instantTaskExecutorRule = InstantTaskExecutorRule()

    @get:Rule
    val coroutineRule = MainCoroutineRule()

    private val networkService = mock<NetworkService>()
    private val sut = PostDetailsViewModel(networkService)

    @Test
    fun `on get author - calls get user details`() = runBlockingTest {
        sut.getAuthor("1")

        verify(networkService).getUserDetails("1")
    }

    @Test
    fun `on get author - and network service succeeds - emits success state`() = runBlockingTest {
        ArrangeBuilder()
            .withUserSuccess("1", userResponse)

        sut.getAuthor("1")

        val viewState = sut.viewState.value

        assertThat(viewState, instanceOf(ViewState.Data::class.java))
    }

    @Test
    fun `on get author - and network service emits network error - emits failure state`() =
        runBlockingTest {
            ArrangeBuilder()
                .withGetAuthorFailed(Result.Failure.NetworkError)

            sut.getAuthor("1")

            val viewState = sut.viewState.value

            assertEquals(ViewState.NetworkError, viewState)
        }

    @Test
    fun `on get author - and network service emits http error - emits failure state`() =
        runBlockingTest {
            ArrangeBuilder()
                .withGetAuthorFailed(Result.Failure.HttpError("Bang!"))

            sut.getAuthor("1")

            val viewState = sut.viewState.value

            assertEquals(ViewState.HttpError("Bang!"), viewState)
        }

    private inner class ArrangeBuilder {
        suspend fun withUserSuccess(userId: String, user: GetUserQuery.Data) = also {
            whenever(networkService.getUserDetails(userId))
                .thenReturn(Result.Success(user))
        }

        suspend fun withGetAuthorFailed(error: Result.Failure) = also {
            whenever(networkService.getUserDetails(any()))
                .thenReturn(error)
        }
    }

    private companion object {
        private val userResponse =
            GetUserQuery.Data(GetUserQuery.User(id = "1", name = "ahmad", username = "mlegy"))

    }
}