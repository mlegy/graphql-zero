package com.melegy.graphql.details.presentation

import com.melegy.graphql.GetUserQuery
import com.melegy.graphql.details.presentaion.UserMapper
import com.melegy.graphql.details.presentaion.UserUiModel
import junit.framework.Assert.assertEquals
import org.junit.Test

internal class UserMapperTest {

    @Test
    fun `map user response to ui model correctly`() {
        val rawUser = GetUserQuery.User(
            id = "1",
            name = "ahmad",
            username = "mlegy"
        )

        val uiUser = UserUiModel(
            id = "1",
            name = "ahmad",
            username = "mlegy"
        )

        assertEquals(uiUser, UserMapper(rawUser))
    }
}
