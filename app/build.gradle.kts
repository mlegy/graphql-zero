plugins {
    id("com.android.application")
    id("kotlin-android")
    id("kotlin-kapt")
    id("kotlin-parcelize")
    id("com.apollographql.apollo").version("2.5.4")
}

android {
    compileSdkVersion(30)
    buildToolsVersion("30.0.3")

    defaultConfig {
        minSdkVersion(19)
        targetSdkVersion(30)
        multiDexEnabled = true
        testInstrumentationRunner("androidx.test.runner.AndroidJUnitRunner")
        consumerProguardFiles("consumer-rules.pro")
    }

    buildFeatures {
        viewBinding = true
    }

    compileOptions {
        sourceCompatibility = JavaVersion.VERSION_1_8
        targetCompatibility = JavaVersion.VERSION_1_8
    }

    kotlinOptions {
        jvmTarget = JavaVersion.VERSION_1_8.toString()
    }
}

apollo {
    generateKotlinModels.set(true)
}

dependencies {
    implementation("org.jetbrains.kotlin:kotlin-stdlib:1.4.31")
    implementation(project(":core"))
    implementation("com.google.android.material:material:1.3.0")
    implementation("androidx.constraintlayout:constraintlayout:2.0.4")

    implementation("com.facebook.shimmer:shimmer:0.5.0")

    implementation("androidx.fragment:fragment-ktx:1.3.0")

    implementation("com.google.dagger:dagger:2.31.2")
    kapt("com.google.dagger:dagger-compiler:2.31")

    implementation("com.apollographql.apollo:apollo-runtime:2.5.4")
    implementation("com.apollographql.apollo:apollo-coroutines-support:2.5.4")

    testImplementation("com.nhaarman.mockitokotlin2:mockito-kotlin:2.2.0")
    testImplementation("android.arch.core:core-testing:1.1.1")
    testImplementation("org.assertj:assertj-core:3.17.2")
    testImplementation("org.jetbrains.kotlinx:kotlinx-coroutines-test:1.4.3")

    androidTestImplementation("androidx.test:runner:1.3.0")
    androidTestImplementation("androidx.test.espresso:espresso-core:3.3.0")
    androidTestImplementation("androidx.test.ext:junit:1.1.2")
    androidTestImplementation("com.schibsted.spain:barista:3.8.0") {
        exclude("org.jetbrains.kotlin")
    }
    androidTestImplementation("com.apollographql.apollo:apollo-idling-resource:2.5.4")
    debugImplementation("androidx.fragment:fragment-testing:1.3.0")
}